import React from 'react';
import {Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'

const useStyle = makeStyles(theme => ({
    pageTitle: {
        marginTop: '1em',
        marginBottom: '1.7em',
        fontWeight: 'bold',
        color: theme.palette.secondary.dark,
        letterSpacing: '3px',
        textTransform: 'uppercase'
    },
    line: {
        height: '1px',
        border: 'none',
        background: theme.palette.secondary.dark
    }
}))


export const PageTitle = ({children}) => {
    const classes = useStyle();
    return (
        <Typography className={classes.pageTitle} align="center" variant="h3">
            {children}
            <hr className={classes.line} width='50px'/>
        </Typography>
    )
}

