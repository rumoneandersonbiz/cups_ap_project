import React from 'react'; 
import { Dialog, DialogContent, makeStyles } from '@material-ui/core';
import loaderGif from '../../assets/images/gif/loaderGif.gif';


const loaderStyles = makeStyles(() => ({
    loaderConatiner: {
        padding: '0',
        background: '#f0e5d5'   
    },
}));

export const Loader = ({open}) => {
    const classes = loaderStyles();

    return (
        <>
          <Dialog
            fullWidth={true}
            maxWidth={"xs"}
            elevation={0}
            open={open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogContent className={classes.loaderConatiner}>
              <img src={loaderGif} style={{width: '100%', heigh: 'auto'}} alt={'loader'} />
            </DialogContent>
          </Dialog>
        </>
    );
}