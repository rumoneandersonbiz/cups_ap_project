import React from 'react';
import Dropzone from 'react-dropzone';
import { Typography, Paper } from '@material-ui/core';
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        border: '0.15em dashed',
        borderColor: theme.palette.secondary.light,
    },
    imageSection: {
        display: 'flex',
        justifyContent: 'center'
    },
    icon: {
        display: 'flex',
        justifyContent: 'center'
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px dashed #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    },
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    },
    img: {
        display: 'block',
        width: 'auto',
        height: '100%',
    },
}))

export const OrderDropzone = ({dropzoneName, fileType, filePreview, addFile, placeholder}) => {
    const classes = useStyles();
    const thumb = () => (
        <div className={classes.thumb} >
          <div className={classes.thumbInner}>
            <img
                alt=''
                src={filePreview}
                className={classes.img}
            />
          </div>
        </div>
      );
    
    return (
        <React.Fragment>
            <Dropzone accept={fileType} onDrop={files => addFile(dropzoneName, files)}>
                {({getRootProps, getInputProps}) => (
                    <Paper className={classes.root} elevation={0}>
                        <div {...getRootProps()}>
                            <input {...getInputProps()} />
                            <Typography variant="subtitle2" align="center" gutterBottom>
                                {placeholder}                                
                            </Typography>
                            <div className={classes.icon}>
                                <CloudUploadOutlinedIcon fontSize='large' color='secondary' />
                            </div>
                            <div className={classes.imageSection}>{thumb()}</div>
                        </div>
                    </Paper>
                )}
            </Dropzone>
        </React.Fragment>
    )
}