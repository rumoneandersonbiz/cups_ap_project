import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {Card, CardHeader, CardMedia, CardContent, Avatar, IconButton, Typography} from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';

import VisibilityIcon from '@material-ui/icons/Visibility';
import HearingIcon from '@material-ui/icons/Hearing';

import { red } from '@material-ui/core/colors';
import VARIABLES from '../../config';

const useStyles = makeStyles(theme => ({
    root: {
        margin: theme.spacing(1),
        width: 300
    },
    media: {
      height: 120,
      paddingTop: theme.spacing(1),
    },
    avatar: {
      backgroundColor: theme.palette.secondary.light
    },
    orderNumber: {
        fontWeight: '100'
    },
    numberStyle: {
        fontWeight: '500'
    },
    deleteIcon: {
        color: red[300]
    }
}));

export const OrderCard = ({completeOrder, order_id, item_name, firstName, lastName, disability, order_time, item_photo}) => {
    const classes = useStyles();

    const handle_time = ()=> new Date(order_time).toLocaleTimeString('en-US');


    const complete = () => {
        completeOrder(order_id)
    }
        
    return (
        <Card className={classes.root}>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                       {disability.toLowerCase() === "blind"? (
                           <VisibilityIcon/>
                       ): (
                           <HearingIcon />
                       )} 
                    </Avatar>
                }
                action={
                    <IconButton onClick={complete} aria-label="settings">
                        <DoneIcon />
                    </IconButton>
                }
                title={item_name}
                subheader={`${firstName} ${lastName}`}
            />
            <CardMedia
                className={classes.media}
                image={`${VARIABLES.url}/${item_photo}`}
                title={item_name}
            />
            <CardContent>
                <Typography variant="h4" color="textSecondary" className={classes.orderNumber}>
                Time <Typography className={classes.numberStyle} component="span" variant="h4">{handle_time()}</Typography>
                </Typography>
            </CardContent>
        </Card>
    )
}
