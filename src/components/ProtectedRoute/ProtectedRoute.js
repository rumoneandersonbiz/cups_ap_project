import React from "react";
import { Route, Redirect } from "react-router-dom";

export const ProtectedRoute = ({
    component: Component,
    layout: Layout,
    isProtected,
    isAuthenticated, 
    isVerifying,
    ...rest
}) => {

    // set authenticated to true if environment is development
    if (process.env.NODE_ENV === 'development') {
        isAuthenticated = true
    }

    if (isProtected) {
        return (<Route 
            {...rest}
            render = { props => isVerifying? 
                (<div/>) : 
                isAuthenticated? 
                    (
                        <Layout {...props}>
                            <Component {...props} />
                        </Layout>
                    ): 
                    (<Redirect to={{pathname:"/login", state: {from:props.location} }}/>)
                }
        />)
    } else {
        return (
            <Route 
                render = {
                    props => (
                        <Layout {...props}>
                            <Component {...props}/>
                        </Layout>
                    )
                }
            />
        )
    }   
};
