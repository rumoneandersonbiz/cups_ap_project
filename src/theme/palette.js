import { colors } from '@material-ui/core';

const white = '#FFFFFF';
const black = '#000000';

export default {
  black,
  white,
  primary: {
    light:"rgba(255, 234, 223, 1)",
    main:"rgba(253, 228, 215, 1)",
    dark:"rgba(228, 159, 123, 1)",
    contrastText:"rgba(90, 83, 83, 1)"
  },
  secondary: {
    light:"rgba(205, 111, 127, 1)",
    main:"rgba(160, 113, 120, 1)",
    dark:"rgba(91, 61, 66, 1)",
    contrastText:"#fff"
  },
  error: {
    light:"#e57373",
    main:"#f44336",
    dark:"#d32f2f",
    contrastText:"#fff"
  },
  text: {
    primary:"rgba(0, 0, 0, 0.87)",
    secondary:"rgba(0, 0, 0, 0.54)",
    disabled:"rgba(0, 0, 0, 0.38)",
    hint:"rgba(0, 0, 0, 0.38)"
  },
  background: {
    default: '#F4F6F8',
    paper: white
  },
  icon: colors.blueGrey[600],
  divider: colors.grey[200]
};

