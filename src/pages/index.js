import {Home} from './Home';
import {Login} from './Login';
import {ManagerOrder} from './ManagerOrder';
import {ManagerInventory} from './ManagerInventory';
import {CustomerHome} from './CustomerHome';
import {CustomerMenu} from './CustomerMenu';

export {
    Home,
    Login,
    ManagerOrder,
    ManagerInventory,
    CustomerHome,
    CustomerMenu
}