import React from 'react';
import { useAlertDialog } from '../../common/CustomAlerts/ProviderCustomAlerts';
import Axios from 'axios';
import VARIABLES from '../../config';

export const InventoryContext = React.createContext();

export const ManagerInventoryProvider = ({children}) => {
    const alert = useAlertDialog();
    const [data, setData] = React.useState([]);
    const [item_photo, setItemPhoto] = React.useState();
    const [item_audio, setItemAudio] = React.useState();
    const [sign_photo, setItemSign] = React.useState();

    const [waiting, setWaiting] = React.useState(false);
 
    const updateItemPhoto = (event) => {
        setItemPhoto(event.target.files[0])
    }

    const updateItemAudio = (event) => {
        setItemAudio(event.target.files[0])
    }

    const updateItemSign = (event) => {
        setItemSign(event.target.files[0])
    }

    const InitialColumns = [
        {
            title: "Item Name", 
            field: "item_name",
            initialEditValue: 'Enter item name'
        },
        {
            title: "Quantity",
            field: "quantity",
            type: "numeric"
        },
        {
            title: "Cost",
            field: "item_cost",
            type: "numeric"
        },
        {
            title: "Category",
            field: "category_id",
            lookup: { 1: 'Beverage', 2: 'Food' }
        },
        {
            title: "Item photo",
            field: "item_photo",
            editComponent: props => (
                <div>
                    <input type="file" name="item_photo" accept="image/*" onChange={updateItemPhoto} />
                </div>
            ),
            render: rowData => <img src={`${VARIABLES.url}/${rowData.item_photo}`} alt="item icon" style={{width: 50, borderRadius: '50%'}}/>,
            editable: 'onAdd'
        },
        {
            title: "Item Sign",
            field: "sign_photo",
            editComponent: props => (
                <input type="file" name="item_sign" accept="image/*" onChange={updateItemSign}/>
            ),
            render: rowData => <img src={`${VARIABLES.url}/${rowData.sign_photo}`} alt="item sign" style={{width: 50, borderRadius: '50%'}}/>,
            editable: 'onAdd'

        },
        {
            title: "Item Audio",
            field: "item_audio",
            editComponent: props => (
                <input type="file" name="item_audio" accept="audio/*" onChange={updateItemAudio} />
            ),
            render: rowData => <audio controls><source src={`${VARIABLES.url}/${rowData.item_audio}`} type="audio/*"/></audio>,
            editable: 'onAdd'
        }
    ]  

    React.useEffect(() => {
        getItems()
    }, [])

    const getItems = () => {
        setWaiting(true)
        
        Axios.get(`${VARIABLES.url}/api/item`)
        .then(res => setData(res.data))
        .catch(() => console.log('error'))
        .finally(setWaiting(false))
    }

    const addRecord = (newRecord) => {
        const recordMedia = {item_photo, item_audio, sign_photo};

        const itemUploadObject = {
            ...newRecord,
            ...recordMedia,
            category_id: 1
        }

        const obj = uploadableObj(itemUploadObject) 

        Axios.post(VARIABLES.url+'/api/item/', obj, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            }).then(res => {
                setData(
                    ...data,
                    res.data
                )
                getItems()
            })
            .catch(() => {
                alert.launchAlert({
                    title: "Error occured",
                    body: "There was an error adding your new item",
                    alertType: "default"
                })
            })
    }

    const editRecord = async (newRecord) => {
        const {item_id, category_id, item_name, item_cost, quantity} = newRecord;      
        const updateObj = {item_id, category_id, item_name, item_cost, quantity}

        try {
            const res = await Axios.post(`${VARIABLES.url}/api/item/${updateObj.item_id}`, updateObj);

            getItems()
        } catch (e) {
            alert.launchAlert({
                title: "Error occured",
                body: "There was an error adding your new item",
                alertType: "default"
            })
        }
    }

    const deleteRecord = async (newRecord) => {
        const {item_id} = newRecord;      

        try {
            const res = await Axios.delete(`${VARIABLES.url}/api/item/${item_id}`);
            
            getItems()
        } catch (e) {
            alert.launchAlert({
                title: "Error occured",
                body: "There was an error adding your new item",
                alertType: "default"
            })
        }
    }


    const uploadableObj = (obj) => {
        let f = new FormData();

        Object.keys(obj).forEach(element => {
            f.append(element, obj[element])
        });

        return f;
    }

    const state = {
        columns: InitialColumns,
        data,
        addRecord,
        editRecord,
        deleteRecord,
        waiting
    }    

    return (
        <InventoryContext.Provider value={state}>
            {children}
        </InventoryContext.Provider>
    )
}