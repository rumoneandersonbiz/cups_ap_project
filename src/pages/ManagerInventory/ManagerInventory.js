import React from 'react';
import {PageTitle} from '../../components/PageTitle';
import { ManagerInventoryProvider } from './ManagerInventoryProvider';
import { InventoryTable } from './InventoryTable';


export const ManagerInventory = () => (
  <>
    <PageTitle>Inventory</PageTitle>
    <ManagerInventoryProvider>
      <InventoryTable />
    </ManagerInventoryProvider>
  </>
);

