import React from 'react';
import MaterialTable from 'material-table';
import { InventoryContext } from './ManagerInventoryProvider';
import { Loader } from '../../components';

export const InventoryTable = () => {
    return (
        <InventoryContext.Consumer>
            {value => (
                <>
                    <Loader open={value.waiting} />
                    <MaterialTable
                        title=""
                        columns={value.columns}
                        data={value.data}
                        editable={{
                            onRowAdd: newData => {
                                return new Promise((resolve) => {
                                    value.addRecord(newData)
                                    resolve()
                                })
                            },
                            onRowUpdate: newData => {
                                return new Promise((resolve) => {
                                    value.editRecord(newData)
                                    resolve()
                                })
                            },
                            onRowDelete: newData => {
                                return new Promise((resolve) => {
                                    value.deleteRecord(newData)
                                    resolve()
                                })
                            }
                        }}
                    />
                </>
            )}
        </InventoryContext.Consumer>   
    )
}