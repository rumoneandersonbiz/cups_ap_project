import React from 'react';
import {Login} from './Login';
import { useSelector, useDispatch } from 'react-redux';
import { loginUser } from '../../actions';



export const LoginContainer = () => {
    const [credentials, setCredentials] = React.useState({});

    // redux connection
    const isLoggingIn = useSelector(state => state.auth.isLoggingIn);
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
    const dispatch = useDispatch()

    const handleSubmit = () => {        
        dispatch(loginUser(credentials));
    };

    const handleChange = ({ target }) => {
        const {name, value} = target;

        setCredentials({
            ...credentials,
            [name]: value
        })
    };


    const LoginProps = {
        isLoggingIn,
        isAuthenticated,
        handleSubmit,
        handleChange
    }
    
    return <Login {...LoginProps} />
}