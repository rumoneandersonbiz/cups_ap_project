import React from 'react'
import { OrderCards } from '../components'
import { PageTitle } from '../../../components/PageTitle'

export const Menu = () => {
    return (
        <div>
            <PageTitle>Menu</PageTitle>
            <OrderCards />
        </div>
    )
}
