import React from 'react'
import Axios from 'axios';
import VARIABLES from '../../config';


export const MenuContext = React.createContext();

export const MenuProvider = ({children}) => {
  // states
  const [menuItems, setMenuItems] = React.useState([]);
  const [selected, setSelected] = React.useState([]);

  const [loading, setLoading] = React.useState(false);

  
  React.useEffect(() => {
    const getItems = async() => {
      setLoading(true)
      try {
        const { data } = await Axios.get(`${VARIABLES.url}/api/item`);
        
        setMenuItems(data);  
        setLoading(false)    
      } catch (e) { 
        // alert.launchAlert({
        //     title: "Error",
        //     body: "There was an error obtaining items",
        //     alertType: "default"
        // })
        setLoading(false)
      }   
    }

    getItems();
  }, [])

  
  // methods
  const handleSelect = (identifier) => {
    // get the currently selected element
    const newItem = menuItems.filter(item => item.item_name === identifier)
    // check if item was already selected
    const checkSelected = selected.filter(item => item.item_name === identifier);
    
    if (!checkSelected.length > 0){
      setSelected([...selected, ...newItem])      
    } else {
      alert.launchAlert({
        title: "Duplicate Selection",
        body: "Item was already selected",
        alertType: "default"
      })
    }
  }

  const removeSelected = (identifier) => {
    const newSelected = selected.filter(item => item.item_id !== identifier);
    setSelected(newSelected);
  }


  // Provider Variables
  const providerObj = {
    menuItems,
    selected,
    handleSelect,
    removeSelected,
    loading,
  }
  
  return (
    <MenuContext.Provider value={{
      ...providerObj
    }} >
        {children}
    </MenuContext.Provider>
  )
}




