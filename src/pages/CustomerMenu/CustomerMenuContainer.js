import React from "react";
import { CustomerMenu } from "./CustomerMenu";
import { CustomerProvider, CustomerContext } from "./CustomerProvider";
import { useAlertDialog } from "../../common/CustomAlerts";
import _ from 'lodash/core'


export const CustomerMenuContainer = () => {
	const alert = useAlertDialog()
	return (
		<CustomerProvider>
			<CustomerContext.Consumer>
				{value => {
					if (!_.isEmpty(value.error)) {
						alert.launchAlert({
							title: 'Message',
							body: value.error,
							type: 'default'
						}, () => value.setError(''))
					}
					return <CustomerMenu />
				}}
			</CustomerContext.Consumer>
		</CustomerProvider>
	)
}