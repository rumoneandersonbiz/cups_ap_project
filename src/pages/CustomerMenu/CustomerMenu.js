import React, { useState } from 'react';
import { Grid, Paper, makeStyles, Button } from '@material-ui/core';
import { CustomerAuth } from './CustomerAuth';
import { CustomerDetail } from './CustomerDetail/CustomerDetail';
import { Menu } from './Menu';
import { SelectedDrawer } from './components';
import { MenuProvider } from './MenuProvider';
import { UploadOrder } from './UploadOrder';
import { CustomerContext } from './CustomerProvider';
import _ from 'lodash/core'

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    root: {
        marginTop: theme.spacing(2)
    }
}))


export const CustomerMenu = () => {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const [openOrder, setOpenOrder] = useState(false)

    const toggle = () => {
        setOpen(!open)
    }

    const toggleNewOrder = () => {
        setOpenOrder(!openOrder)
    }
    return (
        <React.Fragment>
            <Grid container className={classes.root} spacing={3}>
                <Grid item md={3} xs={12}>
                    <Paper className={classes.paper} style={{marginBottom: '1em'}} variant="outlined" elevation={0}>
                        <CustomerAuth />
                    </Paper>
                    <Paper className={classes.paper} variant="outlined" elevation={0}>
                        <CustomerContext.Consumer>
                            {value => (
                                <React.Fragment>
                                    <Button size="small" onClick={() => {
                                    value.getOrders();
                                    toggle()
                                    }} color="secondary" disabled={_.isEmpty(value.customer)}>
                                        View Orders
                                    </Button>
                                    <CustomerDetail hide={value.hide} toggle={toggleNewOrder} />                                
                                    <UploadOrder reset={value.clearReset} open={openOrder} toggle={toggleNewOrder} customer={value.customer} />
                                    <SelectedDrawer open={open} toggle={toggle} />
                                </React.Fragment>
                            )}
                        </CustomerContext.Consumer>
                    </Paper>
                </Grid>
                <MenuProvider>
                    <Grid item md={9} xs={12}>
                        <Paper className={classes.paper} variant="outlined" elevation={0}>
                            <Menu />
                        </Paper>
                    </Grid>
                </MenuProvider>
            </Grid>
        </React.Fragment>
    )
}