import React from 'react'
import { OrderDropzone } from '../../../components'
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import {CustomerContext} from '../CustomerProvider'
import { Button } from '@material-ui/core'
import _ from 'lodash/core'

export const CustomerAuth = () => {
    return (
        <CustomerContext.Consumer>
            {value => {
                const {addImage, upload, preview} = value;
                const dropZoneProps = {
                    addFile: addImage,
                    dropzoneName: 'authPhoto',
                    fileType: 'image/jpeg, image/png',
                    filePreview: preview,
                    placeholder: 'Click or drag authentication image',
                }
                return (
                    <React.Fragment>
                        <OrderDropzone {...dropZoneProps} />
                        <Button
                            variant="outlined"
                            color="secondary"
                            style={{margin: '0.8em'}}
                            startIcon={<CloudUploadIcon />}
                            onClick={upload}
                            disabled={_.isEmpty(preview)}
                        >
                            Upload
                        </Button>
                    </React.Fragment>
                )
            }}
        </CustomerContext.Consumer>
    )
}
