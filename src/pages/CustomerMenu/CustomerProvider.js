import React, { createContext, useState } from 'react'
import { ApiCall } from '../../utils'
import { POST_IMAGE_CUSTOMER_LOGIN, GET_CUSTOMER_ORDERS, GET_CANCEL_CUSTOMER_ORDER } from '../../constants'
import { TooMuchFilesError } from '../../utils/Errors'
import _ from 'lodash/core';
import { useAlertDialog } from '../../common/CustomAlerts';

export const CustomerContext = createContext()

export const CustomerProvider = ({children}) => {

    const [customer, setCustomer] = useState({});
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);
    const [file, setFile] = useState({});
    const [hide, setHide] = useState(false)

    const [orders, setOrders] = useState([])

    const alert = useAlertDialog()

    const addImage = async (name, file) => {
        try {
            if (file.length > 1) {
                throw new TooMuchFilesError('Only one file is requried')
            }

            const firstfile = file[0];
            setError('')
            setFile(Object.assign(firstfile, {preview: URL.createObjectURL(firstfile)}))
        } catch (error) {
            if(_.isEqual(error.name, 'TooMuchFilesError')) {
                setError(error.message)
            } else {
                setError('There was an error with the file uploaded')
            }
        }        
    }

    const clearReset = () => {
        setCustomer({})
        setOrders([])
        setFile({})
        setError('')
        setLoading(false)
    }


    const uploadAuthImage = async () => {
        const Api = new ApiCall(POST_IMAGE_CUSTOMER_LOGIN);
        setLoading(true)
        const result = await Api.postFile('authPhoto', file);

        clearReset()

        if (_.isObject(result)) {
            setCustomer(result)
        } else {
            setError("Authentication failed please check if image uploaded is correct")
        }
        setLoading(false)
    }

    const getOrders = async () => {
        const Api = new ApiCall(`${GET_CUSTOMER_ORDERS}/${customer.customer_id}`)
        const result = await Api.getData()

        if(_.isObject(result)) {
            setOrders(result)
        } else {
            setError("You do not currently have any orders in the system")
        }
    }

    const cancelOrder = async (id) => {
        const Api = new ApiCall(`${GET_CANCEL_CUSTOMER_ORDER}/${id}`)
        const result = await Api.getData()
        
        alert.launchAlert({
            title: 'Order cancelled',
            body: 'Order was cancelled successfully',
            type: 'default'
        })
        setOrders(orders.filter(item => item.order_id !== id))
    }

    const CustomerData = {
        addImage,
        upload: uploadAuthImage,
        clearReset,
        customer,
        error,
        setError,
        loading,
        preview: file.preview,
        orders,
        getOrders,
        cancelOrder,
        hide,
        setHide
    }

    return (
        <CustomerContext.Provider value={{...CustomerData}}>
            {children}
        </CustomerContext.Provider>
    )
}
