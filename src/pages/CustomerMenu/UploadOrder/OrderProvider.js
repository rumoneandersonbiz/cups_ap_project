// Order provider will collect customer information in react context and determine type of user
// multistep order process
import React from 'react';
import Axios from 'axios';
import { useAlertDialog } from '../../../common/CustomAlerts';
import VARIABLES from '../../../config';


export const OrderContext = React.createContext();

export const OrderProvider = (props) => {
    // context store customer information
    const alert = useAlertDialog();

    const [customer, setCustomer] = React.useState({});

    const [error, setError] = React.useState({});
    const [loading, setLoading] = React.useState(false);

    const [itemPhoto, setItemPhoto] = React.useState({});
    const [itemAudio, setItemAudio] = React.useState({})
    const [authPhoto, setAuthPhoto] = React.useState({});

    const addImage = (inputName, files) => {
        // media type stores the name of the media being added
        // method accepts an array of files
        // if length is more than 1 throw error

        if (files.length > 1) {
            setError({inputName: inputName, msg: 'You cannot upload more than one file'});
            return;
        }

        const file = files[0];

        // update state
        if (inputName.toLowerCase() === 'itemphoto'){
            setItemPhoto(Object.assign(file, {preview: URL.createObjectURL(file)}))
        } else if (inputName.toLowerCase() === 'itemaudio') {
            setItemAudio(Object.assign(file, {preview: URL.createObjectURL(file)}))
        } else if (inputName.toLowerCase() === 'authphoto') {
            setAuthPhoto(Object.assign(file, {preview: URL.createObjectURL(file)}))
            getCustomer(file)
        }  
        
    }

    const placeOrder = () => {
        const order = formatUploads();

        const uploadEndpoint = (customer.disability.toLowerCase() === 'blind') ? '/api/order/blind': '/api/order/deaf'

        setLoading(true)
        Axios.post(`${VARIABLES.url}${uploadEndpoint}`, order, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        }).then (() => alert.launchAlert({
                title: "Success",
                body: `Your order was placed successfully`,
                alertType: "default"
            })
        ).catch(() => alert.launchAlert({
            title: "Error",
            body: "There was an error obtaining items",
            alertType: "default"
        }))
        setLoading(false)
        setItemPhoto({})
        setItemAudio({})
        setAuthPhoto({})
        setCustomer({})
    }


    const getCustomer = (file) => {
        const image = new FormData();
        image.append('authPhoto', file);
        setLoading(true)
        
        Axios.post(`${VARIABLES.url}/api/customer/auth`, image, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(({data}) => {
            setCustomer(data);
            console.log(data)
        })
        .catch(() => {
            alert.launchAlert({
                title: "Authentication",
                body: "This authentication image is incorrect",
                alertType: "default"
            })
            setCustomer({})
            setAuthPhoto({})
        })
        

        setLoading(false)
    }

    // put images in format that can be uploaded
    const formatUploads = () => {
        const f = new FormData();
        if (customer.disability.toLowerCase() === 'blind') {
            f.append('itemAudio', itemAudio)
        } else {
            f.append('itemPhoto', itemPhoto)
        }
        f.append('authPhoto', authPhoto)
        return f;
    }

    const value = {
        customer,
        addImage,
        authPhoto,
        itemPhoto,
        itemAudio,
        placeOrder,
        loading,
        error,
    }

    return (
        <OrderContext.Provider value={{...value}}>
            {props.children}
        </OrderContext.Provider>
    )
}