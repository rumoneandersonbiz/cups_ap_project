import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Button, Typography, DialogActions, DialogContent, DialogTitle, Dialog, Container, Stepper, Step, StepLabel } from '@material-ui/core';
import { OrderContext } from './OrderProvider';
import _ from 'lodash/core'
import { CustomerProvider } from '../CustomerProvider';
import { OrderDropzone } from '../../../components';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(1),
    },
    foot:{
        display: 'flex',
        justifyContent: 'center',
        marginTop: theme.spacing(2)
    },
    font: {
        color: theme.palette.secondary.light
    },
    orderBtn: {
        color: theme.palette.secondary.light
    },
    stepper: {
        padding: theme.spacing(3, 0, 2),
    },
    avatar: {
        backgroundColor: theme.palette.secondary.light
    },
}))


export const UploadOrder = ({open, toggle, addItem, disability, authPhoto, item, placeOrder, error}) => {   
    const classes = useStyles();

    const [activeStep, setActiveStep] = useState(0);
    const steps = ['Upload Item', 'Authenticate Customer'];

    const handleNext = () => {
        setActiveStep(activeStep + 1);
    }   

    const resetOrder = () => {
        toggle()
        setActiveStep(0)
    }

    const displayStep = (stepNumber) => {

        switch (stepNumber) {
            case 0:
                const itemDropzoneProps = {
                    addFile: addItem,
                    dropzoneName: _.isEqual(disability, 'blind')? 'itemAudio': 'itemPhoto',
                    fileType: 'image/jpeg, image/png',
                    filePreview: item.file.preview,
                    placeholder: 'Click or drag item media',                    
                }
                return <OrderDropzone {...itemDropzoneProps} />
            case 1: 
                const authDropzoneProps = {
                    addFile: addItem,
                    dropzoneName: 'authPhoto',
                    fileType: 'image/jpeg, image/png',
                    filePreview: authPhoto.preview,
                    placeholder: 'Click or drag authentication image',                    
                }
                return <OrderDropzone {...authDropzoneProps} />
            default:
                return <div>There is an error</div>
        }
    }

    return (
        <React.Fragment>
            <CustomerProvider>
                <Dialog
                    open={open}
                    fullWidth={true}
                    maxWidth={'sm'}
                    onClose={resetOrder}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                    <Typography align="center"  variant="h5" gutterBottom>
                        Custom Order
                    </Typography>        

                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map((label) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>

                    </DialogTitle>
                    <DialogContent>
                        <Container>
                            {displayStep(activeStep)}
                        </Container>
                        <Typography variant="caption" display="block" style={{marginTop: '1em'}} gutterBottom>
                            {error}
                        </Typography>
                    </DialogContent>

                    <DialogActions>
                        <OrderContext.Consumer>
                            {value => (
                                <>
                                    <Button color="secondary" onClick={resetOrder}>
                                        Cancel
                                    </Button>                                
                                    {activeStep === 0 && <Button color="secondary" disabled={_.isEmpty(item.file)} onClick={handleNext}>
                                        Next
                                    </Button>}
                                    <Button variant="outlined" color="secondary" disabled={_.isEmpty(authPhoto)} onClick={placeOrder} className={classes.orderBtn}>
                                        Place Order
                                    </Button>
                                </>
                            )}
                        </OrderContext.Consumer>
                    </DialogActions>                    
                </Dialog> 
            </CustomerProvider>          
        </React.Fragment>
    )
}



