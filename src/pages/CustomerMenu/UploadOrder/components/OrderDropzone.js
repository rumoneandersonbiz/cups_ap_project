import React from 'react';
import Dropzone from 'react-dropzone';
import { Typography, Paper } from '@material-ui/core';
import CloudUploadOutlinedIcon from '@material-ui/icons/CloudUploadOutlined';
import { makeStyles } from '@material-ui/styles';
import {OrderContext} from '../OrderProvider';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        border: '0.15em dashed',
        borderColor: theme.palette.secondary.light,
    },
    imageSection: {
        display: 'flex',
        justifyContent: 'center'
    },
    icon: {
        display: 'flex',
        justifyContent: 'center'
    },
    thumb: {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px dashed #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    },
    thumbInner: {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    },
    img: {
        display: 'block',
        width: 'auto',
        height: '100%'
    }
}))

export const OrderDropzone = ({inputName}) => {
    const classes = useStyles();

    const thumb = (preview) => (
        <div className={classes.thumb} >
          <div className={classes.thumbInner}>
            <img
                alt=''
                src={preview}
                className={classes.img}
            />
          </div>
        </div>
      );
    
    return (
        <OrderContext.Consumer>
            {value => (
                <>
                    <Dropzone onDrop={files => value.addImage(inputName, files)}>
                        {({getRootProps, getInputProps}) => (
                            <Paper className={classes.root} elevation={0}>
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <Typography variant="subtitle2" align="center" gutterBottom>
                                        Drag and drop
                                        {(inputName.toLowerCase() === "authphoto") && <span style={{fontWeight: 'bold'}}> Authentication Image</span>}
                                        {(inputName.toLowerCase() === "itemphoto") && <span style={{fontWeight: 'bold'}}> Item Image</span>}
                                        {(inputName.toLowerCase() === "itemaudio") && <span style={{fontWeight: 'bold'}}> Item Audio</span>}
                                    </Typography>
                                    <div className={classes.icon}>
                                        <CloudUploadOutlinedIcon fontSize='large' color='secondary' />
                                    </div>
                                    {/* <div className={classes.imageSection}>{thumb(value[inputName].preview)}</div> */}
                                </div>
                            </Paper>
                        )}
                    </Dropzone>
                    {/* <Typography variant="caption" display="block" style={{color: 'red'}} gutterBottom>{inputName === value.error.inputName ? value.error.msg: ''}</Typography> */}
                </>
            )}
        </OrderContext.Consumer>
    )
}