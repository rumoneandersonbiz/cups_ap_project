import React, { useState } from 'react'
import { UploadOrder } from './UploadOrder'
import { TooMuchFilesError, AuthMediaIncorrect } from '../../../utils/Errors'

import _ from 'lodash/core'
import { ApiCall } from '../../../utils'
import { POST_ORDER_BLIND, POST_ORDER_DEAF, POST_IMAGE_CUSTOMER_AUTH } from '../../../constants'
import { useAlertDialog } from '../../../common/CustomAlerts'

export const UploadOrderContainer = ({open, toggle, customer, reset}) => {

    const [item, setItem] = useState({
        name: '',
        file: {}
    })
    const [authCounter, setAuthCounter] = useState(3);
    const [authPhoto, setAuthPhoto] = useState({});
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);

    const alert = useAlertDialog();

    const addItem = async (name, file) => {
        setError('')

        try {
            if (file.length > 1) {
                throw new TooMuchFilesError('Only one file of the gesture is required')
            }
            const firstfile = file[0];

            if (_.isEqual(name, 'authPhoto')) {
                const Api = new ApiCall(POST_IMAGE_CUSTOMER_AUTH);
                const result = await Api.postFile('authPhoto', firstfile)

                if (_.isEqual(result, customer)) {
                    setAuthPhoto(Object.assign(firstfile, {preview: URL.createObjectURL(firstfile)}))   
                    setError('')                 
                } else {
                    throw new AuthMediaIncorrect()
                } 
                
            } else {
                setItem({
                    name,
                    file: Object.assign(firstfile, {preview: URL.createObjectURL(firstfile)})
                })

                // TODO check if item exist
            }
            
        } catch (error) {
            if (_.isEqual(error.name, 'TooMuchFilesError')) {
                setError(error.message)
            } else if (_.isEqual(error.name, 'AuthMediaIncorrect')){
                if (authCounter !== 1) {
                    setError("Your Authentication image is not correct")
                    setAuthCounter(authCounter -1);
                } else {
                    reset()
                    clearAndToggle()
                    setError("System Reset")
                }
            } else {
                setError('An error occured internally')
            }
        }
    }

    const clearAndToggle = () => {
        toggle()
        setAuthCounter(3);
        setAuthPhoto({});
        setItem({
            name: '',
            file: {}
        })
        setError('');
        setLoading(false)
    }

    const placeOrder = async () => {
        try {
            const Api = new ApiCall(_.isEqual(customer.disability, 'blind')? POST_ORDER_BLIND: POST_ORDER_DEAF);
            const result = await Api.postFiles([item, {name: 'authPhoto', file: authPhoto}])
            alert.launchAlert({
                title: 'Order placed',
                body: 'Order was successfully placed',
                type: 'default'
            })
            clearAndToggle()
            
        } catch (error) {
            setError(error)
        }
    }

    const UploadOrderProps = {
        addItem,
        placeOrder,
        item,
        authPhoto,
        disbility: customer.disability,
        toggle: clearAndToggle,
        open,
        error,
        loading
    }

    return (
        <React.Fragment>
            <UploadOrder {...UploadOrderProps} />
        </React.Fragment>
    )
}
