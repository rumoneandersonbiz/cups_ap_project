import React from 'react'
import { SelectedCard } from './SelectedCard'
import { CustomerContext } from '../../CustomerProvider'

export const SelectedItems = () => {
    return (
        <CustomerContext.Consumer>
            {({orders, cancelOrder}) => (
                orders.map(({order_id, item_name, item_cost}) => (
                    <SelectedCard key={order_id} name={item_name} cost={item_cost} id={order_id} cancel={cancelOrder} />
                ))
            )}
        </CustomerContext.Consumer>
    )
}
