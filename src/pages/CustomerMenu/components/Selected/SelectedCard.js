import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    margin: '1em'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export const SelectedCard = ({id, name, cost, cancel}) => {
  const classes = useStyles();

  const removeMe = () => {
    cancel(id)
  }

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {name}
        </Typography>
        <Typography variant="h4" gutterBottom>
          $ {cost}
        </Typography>
        <Button onClick={removeMe} >Cancel Order</Button>
      </CardContent>
    </Card>
  );
}
