import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import { PageTitle } from '../../../../components/PageTitle';
import { SelectedItems } from './SelectedItems';

const drawerWidth = 360;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
}));

export const SelectedDrawer = ({open, toggle}) => {
  const classes = useStyles();
  
  return (
    <Drawer
    open={open}
    className={classes.drawer}
    anchor="right"
    onClose={toggle}
    classes={{
      paper: classes.drawerPaper,
    }}
    >
      <div className={classes.toolbar} />
      <PageTitle>
          Orders
      </PageTitle>
      <SelectedItems/>

      {/* place orders button */}
    </Drawer>
  );
}
