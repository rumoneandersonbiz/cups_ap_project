import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import VARIABLES from '../../../../config';


const useStyles = makeStyles({
  root: {
    minWidth: 250,
    margin: '1em',
    textAlign: 'left',
  },
  media: {
    height: 180,
  },
});

export const OrderCard = ({photo, name, category, cost, select}) => {
  const classes = useStyles();

  return (
    <Card variant="outlined" className={classes.root}>
        <CardMedia
          className={classes.media}
          image={`${VARIABLES.url}/${photo}`}
          title=""
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {category}
          </Typography>
          <Typography variant="h2" style={{ fontWeight: '100' }} gutterBottom>
            ${cost}
          </Typography>
        </CardContent>
    </Card>
  );
}
