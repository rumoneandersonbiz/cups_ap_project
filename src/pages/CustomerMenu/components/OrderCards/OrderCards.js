import React from 'react'
import { OrderCard } from './OrderCard'
import { makeStyles } from '@material-ui/styles';
import { Container, Typography } from '@material-ui/core';
import {MenuContext} from "../../MenuProvider";

const useStyles = makeStyles({
    orderContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
});

export const OrderCards = () => {
    const classes = useStyles();

    return(
        <MenuContext.Consumer>
            {(value) => {
                return (
                    <Container className={classes.orderContainer}>
                        {value.menuItems.length < 1? (
                            <Typography variant="h5" gutterBottom>
                                We currently have no items
                            </Typography>
                        ):(
                            value.menuItems.map(item => (
                                <OrderCard key={item.item_id} name={item.item_name} photo={item.item_photo} category={item.category} cost={item.item_cost} select={value.handleSelect}/>
                            ))
                        )}
                    </Container>
                )
            }}
        </MenuContext.Consumer>
    )
};
