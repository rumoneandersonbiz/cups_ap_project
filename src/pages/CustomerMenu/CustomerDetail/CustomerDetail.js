import React, { useState } from 'react'
import { Typography, Avatar, makeStyles, Button, IconButton } from '@material-ui/core'
import VisibilityIcon from '@material-ui/icons/Visibility';
import HearingIcon from '@material-ui/icons/Hearing';
import { CustomerContext } from '../CustomerProvider';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';

import _ from 'lodash/core'

const useStyles = makeStyles(theme => ({
    avatar: {
        backgroundColor: theme.palette.secondary.light
    },
    disabilitySection: {
        display: 'flex',
        justifyContent: 'center'
    },
    section: {
        marginTop: '2em'
    },
    orderBtn: {
        color: theme.palette.secondary.light,
        width: '100%'
    },
}))

export const CustomerDetail = ({toggle}) => {
    const classes = useStyles();

    const [hide, setHide] = useState(false)

    const toggleHide = () => {
        setHide(!hide)
    }
    
    return (
        <CustomerContext.Consumer>
            {value => {
                const { firstName, lastName, disability, cups_coin } = value.customer;
                if (_.isEmpty(value.customer)) {
                    return (<p>Please input your auth image above</p>)
                } else {
                    return (
                        <React.Fragment>
                            <div className={classes.section}>
                                <Typography variant="h5" gutterBottom>
                                    Customer Name:
                                </Typography>
                                <Typography variant="overline" display="block" gutterBottom>
                                    {firstName} {lastName}
                                </Typography>
                            </div>

                            <div className={classes.section}>
                                <Typography variant="h5" gutterBottom>
                                    Disability:
                                </Typography>
                                <div className={classes.disabilitySection}>
                                    <Avatar aria-label="recipe" className={classes.avatar}>
                                        {disability.toLowerCase() === "blind" ? (
                                            <VisibilityIcon />
                                        ) : (
                                                <HearingIcon />
                                            )}
                                    </Avatar>
                                </div>
                                <Typography variant="overline" style={{ marginTop: '0.4em' }} display="block" gutterBottom>
                                    {disability}
                                </Typography>
                            </div>

                            <div className={classes.section}>
                                <Typography variant="h5" gutterBottom>
                                    Cups Coins:
                                </Typography>
                                {hide && <Typography variant="h2" style={{ fontWeight: '100' }} gutterBottom>
                                    ${cups_coin}
                                </Typography>}
                                <div>
                                    <IconButton onClick={toggleHide} aria-label="settings">
                                        {hide? <VisibilityOffOutlinedIcon/> :<VisibilityOutlinedIcon /> }
                                    </IconButton> 
                                </div>
                            </div>

                            <div className={classes.section}>
                                <Button variant="outlined" color="secondary" onClick={toggle} size="medium" className={classes.orderBtn}>
                                    New Order
                                </Button>
                            </div>
                        </React.Fragment>
                    )
                }
            }}
        </CustomerContext.Consumer>
    )
}
