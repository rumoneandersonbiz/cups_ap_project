import React from 'react';
import {Home} from './Home';
import { ApiCall } from '../../utils';
import { GET_SALES_REPORT, GET_CATEGORY_REPORT } from '../../constants';

import _ from 'lodash/core'


const doughnutColours = {
    backgroundColor: [
      '#FF6384',
      '#36A2EB',
    ],
    hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
    ] 
  }

export const HomeContainer = () => {
    const [data, setData] = React.useState({});
    const [sales, setSales] = React.useState({});
    const [error, setError] = React.useState(false);
    const [loading, setLoading] = React.useState(false);


    React.useEffect(() => {
        setLoading(true)
        Promise.all([getSalesData(), getCategoryData()])
        .then(() => setLoading(false))
    },[])  


    const getSalesData = async () => {
        const Api = new ApiCall(GET_SALES_REPORT);
        const results = await Api.getData();

        if (_.isObject(results)) {
            setSales(results)
        } else {
            setError(results)
        }
    }

    const getCategoryData = async () => {
        const Api = new ApiCall(GET_CATEGORY_REPORT);
        const results = await Api.getData();

        if(_.isObject(results)) {
            const {food, beverage} = results;
            
            const labels = ['Food', 'Beverage'];
            const values = [food, beverage];

            const reportDataObj = {
                labels,
                datasets: [{
                    data: values,
                    ...doughnutColours
                }],
            }

            setData(reportDataObj)
        } else {
            setError(results)
        }
    }


    const HomeProps = {
        data,
        sales,
        error,
        loading,
    }
    

    return (
        <Home {...HomeProps} />
    )
}