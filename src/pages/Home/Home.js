import React from "react";
import {Container, Grid, Paper, Typography, List, ListItem, ListItemAvatar, Avatar, ListItemText} from '@material-ui/core';
import { Doughnut } from 'react-chartjs-2';
import AttachMoneySharpIcon from '@material-ui/icons/AttachMoneySharp';

import {PageTitle} from '../../components/PageTitle';
import { makeStyles } from "@material-ui/styles";
import _ from 'lodash/core';
import { Loader } from "../../components";


const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  avatar: {
    backgroundColor: "#56ae5a"
  },
}))

export const Home = ({loading, error, sales, data}) => {
  const classes = useStyles(); 


  const listSales = () => {
    // const {reportArr} = sales;
    if (!_.isEmpty(sales)) {
      return sales.reportArr.map((item, id) => (
        <ListItem key={id}>
          <ListItemAvatar>
            <Avatar className={classes.avatar}>
              < AttachMoneySharpIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={`Amount of ${item.item_name} sold`} secondary={item.item_count} />
        </ListItem>
      ))
    }

    return <React.Fragment/>    
  }

  

  return (
    <>
        <Loader open={loading} />
        <PageTitle>
          Dashboard
        </PageTitle>
        <Container>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Doughnut data={data}/>
            </Grid>
            <Grid item xs={6}>
              <Paper className={classes.paper}>
                <Typography variant="h5" gutterBottom>
                  Sales Summary
                </Typography>

                <div>
                  <Typography align="left" variant="overline" display="block" gutterBottom>
                    Number of Sales:
                  </Typography>
                  <hr/>
                  {!loading && <Typography variant="h2" gutterBottom>
                    {`${sales.numSales}`}
                  </Typography>}
                </div>

                <div>
                  <Typography align="left" variant="overline" display="block" gutterBottom>
                    Total amount made:
                  </Typography>
                  <hr/>
                  {!loading && <Typography variant="h2" gutterBottom>
                    {`$ ${sales.salesAmt}.00 `}
                  </Typography>}
                </div>
              </Paper>
            </Grid>
          </Grid>
          <br/>
          <br/>
          <Paper className={classes.paper}>
            <Typography variant="h5" gutterBottom>
              Sales by item
            </Typography>

            <div>
              <List className={classes.root}>
                {listSales()}
              </List>
            </div>
          </Paper>
        </Container>
    </>
  )
}
