import React from 'react'
import { Typography, Grid, Button, Container } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    heroContent: {
        padding: theme.spacing(10, 0, 2),
        backgroundImage: 'url(https://images.unsplash.com/photo-1493606278519-11aa9f86e40a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
          theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: '81vh',
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
}))

export const HeroSection = ({signUp}) => {

    const classes = useStyles();

    return (
        <div className={classes.heroContent}>
            <Container maxWidth="md">
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    CUPS Cafe
                </Typography>

                <Typography  variant="h5" align="center" color="textPrimary" gutterBottom>
                    Cups Uplifting People
                </Typography>

                <div className={classes.heroButtons}>
                    <Grid container spacing={2} justify="center">
                        <Grid item>
                            <Button to="/menu" variant="outlined" color="secondary">
                                Make an order
                            </Button>                                
                        </Grid>
                        <Grid item>
                            <Button variant="contained" onClick={signUp} color="secondary">
                                Sign up
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            </Container>
        </div>
    )
}
