import React from 'react'
import { HeroSection } from './HeroSection'
import { useAlertDialog } from '../../common/CustomAlerts'

import { useSelector } from 'react-redux';
import { Loader } from '../../components';

const CustomerHome = () => {
    const alert = useAlertDialog();

    const customerRequestSignup = useSelector(state => state.customer.requestAuth)

    const handleSignUp = () => {
        alert.launchAlert({
            type: 'signup'
        })
    }
    
    return (
        <main>
            <Loader open={customerRequestSignup} />            
            <HeroSection signUp={handleSignUp} />
        </main>
    )
}

export default CustomerHome
