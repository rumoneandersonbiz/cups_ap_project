import React from 'react'
import { ManagerOrderContext } from './ManagerOrderProvider'
import { OrderCard } from '../../components/OrderCard'
import { Loader } from '../../components'

export const ManagerOrders = () => {
    return (
        <ManagerOrderContext.Consumer>
            {value => (
                <>
                    <Loader open={value.requestingOrders} />
                    {value.orders.map(order => (
                        <OrderCard key={order.order_id} completeOrder={value.completeOrder} {...order} />
                    ))}
                </>
            )}
        </ManagerOrderContext.Consumer>
    )
}
