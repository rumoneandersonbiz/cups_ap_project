import React from 'react';
import {Container, Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/styles';

import {PageTitle} from '../../components/PageTitle';
import { ManagerOrderProvider } from './ManagerOrderProvider';
import { ManagerOrders } from './ManagerOrders';

const useStyles = makeStyles(() => ({
    orderContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    headingBlock: {
        display: 'c'
    }
}))

const ManagerOrder = () => {
    const classes = useStyles()

    return (
        <>
            <PageTitle>Orders</PageTitle>
            <Typography variant="h4" align="center" className={classes.headingBlock} gutterBottom>
                Orders today
            </Typography>
            <Container className={classes.orderContainer}>
                <ManagerOrderProvider>
                    <ManagerOrders />
                </ManagerOrderProvider>
            </Container>
        </>
    )
}

export default ManagerOrder
