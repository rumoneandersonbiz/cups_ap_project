import React from 'react'
import Axios from 'axios';
import VARIABLES from '../../config';
import { useAlertDialog } from '../../common/CustomAlerts/ProviderCustomAlerts';

export const ManagerOrderContext = React.createContext();

export const ManagerOrderProvider = ({children}) => {
    const alert = useAlertDialog();

    // state
    const [orders, setOrders] = React.useState([]);
    const [requestingOrders, setRequestingOrders] = React.useState(false);

    const getOrders = async () => {
        setRequestingOrders(true)
        try{
            const response = await Axios.get(`${VARIABLES.url}/api/order`);
            setOrders(response.data)
        } catch (e) {
            alert.launchAlert({
                title: "Error occured",
                body: "There was an error retreiving the orders please reload the page",
                alertType: "default"
            })
        } finally {
            setRequestingOrders(false)
        }
    }
    
    const completeOrder = async (id) => {
        try {
            const res = await Axios.get(`${VARIABLES.url}/api/order/complete/${id}`);
            
            
            alert.launchAlert({
                title: "Order complete",
                body: "Order status was updated",
                alertType: "default"
            }) 
            getOrders()

        } catch (error) {
            alert.launchAlert({
                title: "Order Not complete",
                body: "Order status was not updated",
                alertType: "default"
            })            
        }
    }

    // get all order currently in the system
    React.useEffect(() => {
        getOrders()
    }, [])  
    


    // provider Object
    const providerObj = {
        orders,
        requestingOrders,
        completeOrder
    }

    return (
        <ManagerOrderContext.Provider value={providerObj}>
            {children}
        </ManagerOrderContext.Provider>
    )
}
