import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { composeWithDevTools } from 'redux-devtools-extension';

// import { verifyAuth } from "./actions";
import rootReducer from "./reducers";

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['customer']
}


export default function configureStore(preloadedState) {

    const persistedReducer = persistReducer(persistConfig, rootReducer);

    const store = createStore(
        persistedReducer, 
        composeWithDevTools(applyMiddleware(thunkMiddleware))
    );

    const persistor = persistStore(store)

    return {store, persistor};
}