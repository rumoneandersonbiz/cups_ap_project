import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { Router } from "react-router-dom";

import App from './App';
import * as serviceWorker from './serviceWorker';

import configureStore from "./configureStore";
import Axios from 'axios';

import {ThemeProvider} from '@material-ui/styles';

import {ProviderCustomAlerts} from './common';

import { PersistGate } from 'redux-persist/integration/react';

// theme
import theme from './theme';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const {store, persistor} = configureStore()

// attach authorization header
Axios.interceptors.request.use(config => {
    const token = store.getState().auth.userIdToken;
    config.headers.authToken = token;

    return config
})



ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <ThemeProvider theme={theme}>
                <Router history={history}>
                    <ProviderCustomAlerts>
                        <App/>
                    </ProviderCustomAlerts>
                </Router>
            </ThemeProvider>
        </PersistGate>
    </Provider>, 
    document.getElementById('root'));

serviceWorker.unregister();
