import React from "react";
import { Switch } from "react-router-dom";
import { connect } from "react-redux";

// components
import {ProtectedRoute} from "./components";

// routes
import { routes } from "./routes";


function App(props) {
  const { isAuthenticated, isVerifying } = props;
  
  return (
    <Switch>
      {routes.map((route, id) => (
        <ProtectedRoute 
          key={id}
          exact
          {...route}
          isAuthenticated={isAuthenticated}
          isVerifying={isVerifying}
        />
      ))}
    </Switch>
  );
}


const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  isVerifying: state.auth.isVerifying
}) 


export default connect(mapStateToProps)(App)
