// API CONSTANTS 
// These are routes the full url is appended when the request is about to be sent
export const GET_SALES_REPORT = '/api/report/sales'
export const GET_CATEGORY_REPORT = '/api/report/category'

// this needs param
export const POST_IMAGE_CUSTOMER_AUTH = '/api/customer/auth'
// this should not have a param
export const POST_IMAGE_CUSTOMER_LOGIN = '/api/customer/auth'

export const POST_ORDER_BLIND = '/api/order/blind'
export const POST_ORDER_DEAF = '/api/order/deaf'

// this needs a param of id=? 😁
export const GET_CUSTOMER_ORDERS = '/api/order'
export const GET_CANCEL_CUSTOMER_ORDER = '/api/order/cancel'