const VARIABLES = {
    url: (process.env.NODE_ENV === 'development')? 'http://localhost:3000' : 'https://cups-ap-project.herokuapp.com'
}

export default VARIABLES;