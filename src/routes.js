import {
    Home,
    Login,
    ManagerOrder,
    ManagerInventory,
    CustomerHome,
    CustomerMenu
} from './pages';
import {ManagerDashLayout, DefaultLayout, CustomerLayout} from './layouts';


export const routes = [
    {
        path: "/",
        component: CustomerHome,
        isProtected: false,
        layout: CustomerLayout
    },
    {
        path: "/menu",
        component: CustomerMenu,
        isProtected: false,
        layout: CustomerLayout
    },
    {
        path: "/dashboard",
        component: Home,
        isProtected: true,
        layout: ManagerDashLayout
    },
    {
        path: "/orders",
        component: ManagerOrder,
        isProtected: true,
        layout: ManagerDashLayout
    },
    {
        path: "/inventory",
        component: ManagerInventory,
        isProtected: true,
        layout: ManagerDashLayout
    },
    {
        path: "/Login",
        component: Login,
        isProtected: false,
        layout: DefaultLayout
    }
]