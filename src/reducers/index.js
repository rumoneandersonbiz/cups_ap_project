import { combineReducers } from 'redux';

import auth from './auth';
import customer from './customer';

const rootReducer = combineReducers({
    auth,
    customer
})

export default rootReducer;