import {
    CUSTOMER_AUTH_SUCCESS,
    CUSTOMER_AUTH_REQUEST,
    CUSTOMER_AUTH_FAILED
} from '../actions/';


const initialState = {
    authSuccess: false,
    authFailed: false,
    requestAuth: false,
    customerId: '',
}




export default ( state = initialState, action) => {
    switch (action.type) {
        case CUSTOMER_AUTH_REQUEST:
            return {
                ...state,
                authSuccess: false,
                requestAuth: true,
                authFailed: false,
            }
        case CUSTOMER_AUTH_SUCCESS:
            return {
                ...state,
                authSuccess: true,
                requestAuth: false,
                authFailed: false,
                customerId: action.customerId
            }
        case CUSTOMER_AUTH_FAILED:
            return {
                ...state,
                authSuccess: false,
                requestAuth: false,
                authFailed: true
            }
        default:
            return state;
    }
}