import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS,
    LOGOUT_FAILURE,
    VERIFY_REQUEST,
    VERIFY_SUCCESS,
    TOKEN_REQUEST,
    TOKEN_SUCCESS,
    TOKEN_FAILED
} from '../actions/';


const initialState = {
    isLoggingIn: false,
    isLoggingOut: false,
    isVerifying: false,
    loginError: false,
    logoutError: false,
    isAuthenticated: false,
    user: {},
    userIdToken: ""
}



export default ( state = initialState, action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                isLoggingIn: true,
                loginError: false
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoggingIn: false,
                loginError: false,
                isAuthenticated: true,
                user: action.user
            }
        case LOGIN_FAILURE:
            return {
                ...state,
                isLoggingIn: false,
                isAuthenticated: false,
                loginError: true
            }
        case LOGOUT_REQUEST:
            return {
                ...state,
                isLoggingOut: true,
                isAuthenticated: true,
                logoutError: false
            }
        case LOGOUT_SUCCESS:
            return {
                ...state,
                isLoggingOut: false,
                isAuthenticated: false,
                logoutError: false,
                user: {}
            }
        case LOGOUT_FAILURE:
            return {
                ...state,
                isLoggingOut: false,
                isAuthenticated: true,
                logoutError: true
            }
        case VERIFY_REQUEST:
            return {
                ...state,
                isVerifying: true,
            }
        case VERIFY_SUCCESS:
            return {
                ...state,
                isVerifying: false,
            }
        case TOKEN_REQUEST:
            return {
                ...state,
                isVerifying: true,
            }
        case TOKEN_SUCCESS:
            return {
                ...state,
                isVerifying: false,
                userIdToken: action.token,
            }
        case TOKEN_FAILED:
            return {
                ...state,
                isVerifying: false
            }
        default:
            return state;
    }
}