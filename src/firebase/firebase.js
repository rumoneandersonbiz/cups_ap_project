import * as firebase from "firebase/app";
import "firebase/auth";

var firebaseConfig = {
    apiKey: "AIzaSyAA4U8y48PhO_r7xmCXAHtFJf374rrt3LA",
    authDomain: "cups-e78c9.firebaseapp.com",
    databaseURL: "https://cups-e78c9.firebaseio.com",
    projectId: "cups-e78c9",
    storageBucket: "cups-e78c9.appspot.com",
    messagingSenderId: "398990741363",
    appId: "1:398990741363:web:591ed2528ce7501efb7f06"
};

const ReactFirebaseInst = firebase.initializeApp(firebaseConfig);
export default ReactFirebaseInst;
