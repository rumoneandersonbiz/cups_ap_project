import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Typography, Button } from '@material-ui/core';
import Axios from 'axios';
import VARIABLES from '../../../../../../config';
import { useAlertDialog } from '../../../../../../common/CustomAlerts/ProviderCustomAlerts';

// import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = props => {
  const { className, ...rest } = props;

  const alert = useAlertDialog()
  const [manager, setManager] = useState({});
  const classes = useStyles();

  // get the current admin
  React.useEffect(() => {
    getAdmin();
  }, [])
  
  const getAdmin = async() => {
    try {
      const res = await Axios.get(`${VARIABLES.url}/api/manager`);

      if (!res) {
        alert.launchAlert({
          title: 'No Current Managers',
          body: 'No current managers are found',
          alertType: 'default'
        })  
      } else {
        const {firstName, lastName} = res.data

        setManager({
          name: `${firstName} ${lastName}`,
          bio: 'Store Manager'
        })
      }

      
      
    } catch (e) {
      alert.launchAlert({
        title: 'Error occured',
        body: 'An error occured while retreiving manager information',
        alertType: 'default'
      })  
    }
  }


  const updateManager = () => {
    alert.launchAlert({
      type: 'Manager'
    }, () => getAdmin()) 
  }

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Typography
        className={classes.name}
        variant="h4"
      >
        {manager.name}
      </Typography>
      <Typography variant="body2">{manager.bio}</Typography>
        <Button variant="text" color="secondary" onClick={updateManager} size="small">
            Update Manager
        </Button>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
