import React from 'react'

export const DefaultLayout = ({children}) => {
    return (
        <React.Fragment>
            {children}
        </React.Fragment>        
    )
}
