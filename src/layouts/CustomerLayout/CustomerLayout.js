import React from 'react'
import {Topbar} from './Topbar';

export const CustomerLayout = ({children}) => {
    return (
        <>
            <Topbar />
            <div>
                {children}
            </div>
        </>
    )
}