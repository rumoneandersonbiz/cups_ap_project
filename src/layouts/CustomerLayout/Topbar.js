import React from 'react'
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import {Link as RouterLink} from 'react-router-dom';
import { CustomerNavigation, CustomerSidebar } from './components';
import clsx from 'clsx';

import MenuIcon from '@material-ui/icons/Menu'
import useMediaQuery from '@material-ui/core/useMediaQuery';


const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      background: 'white',
      borderBottomWidth: '2px',
      borderBottomStyle: 'solid',
      borderBottomColor: theme.palette.primary.light
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1,
      fontWeight: '100',
    },
}));


export const Topbar = () => {
    const classes = useStyles();
    const [sidebarOpen, setSidebarOpen] = React.useState(false);
    const displaySize = useMediaQuery('(min-width:700px)')

    const navigationItems = [
        {
            name: 'Menu',
            href: '/menu',
            variant: 'text'
        },
        {
            name: 'About',
            href: '/',
            variant: 'text'
        },
        {
            name: 'Contact',
            href: '/',
            variant: 'text'
        },
        {
            name: 'Admin Login',
            href: '/login',
            variant: 'outlined',
            color: 'secondary'
        }
    ]

    const toggleSidebar = () => {
        setSidebarOpen(!sidebarOpen)
    }
    
    return (
        <AppBar className={classes.root} elevation={0} position='sticky'>
            <Toolbar>
                <RouterLink to="/" className={classes.menuButton}>
                    <img
                    alt="Logo"
                    src={require('../../assets/images/cupsLogoNoBackgroundCropped.png')}
                    height={50}
                    />              
                </RouterLink>
                <Typography className={classes.title} variant="h4">
                    CUPS Cafè
                </Typography>
                <div>
                    {displaySize?(
                        <CustomerNavigation links={navigationItems} />
                        
                    ):(
                        <div>
                            <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={toggleSidebar}
                            edge="start"
                            className={clsx(classes.menuButton, sidebarOpen && classes.hide)}
                            >
                                <MenuIcon />
                            </IconButton>
                            <CustomerSidebar options={navigationItems} open={sidebarOpen} toggle={toggleSidebar} />
                        </div>
                    )}
                </div>
            </Toolbar>
        </AppBar>
    )
}
