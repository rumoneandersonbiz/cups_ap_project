import React from 'react'
import { Button } from '@material-ui/core'
import { Link as RouterLink } from 'react-router-dom';

export const CustomerNavigation = ({links}) => {
    return (
        <>
            {links.map(link => (
                <Button 
                key={link.name} 
                color={link.color? link.color: 'inherit'} 
                component={RouterLink} 
                variant={link.variant}
                to={link.href} >{link.name}</Button>
            ))}
        </>
    )
}
