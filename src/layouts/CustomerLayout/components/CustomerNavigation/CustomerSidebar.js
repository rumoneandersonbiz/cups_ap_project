import React, {forwardRef} from 'react'
import clsx from 'clsx';
import { Drawer, List, ListItem, Button, colors } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import {NavLink as RouterLink} from 'react-router-dom';


const useStyles = makeStyles(theme => ({
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
    active: {
        color: '#776274',
        fontWeight: theme.typography.fontWeightBold,
    },
    button: {
        color: colors.blueGrey[800],
        padding: '10px 8px',
        justifyContent: 'flex-start',
        textTransform: 'none',
        letterSpacing: 0,
        width: '100%',
        fontWeight: theme.typography.fontWeightLight
    }
}));


const CustomRouterLink = forwardRef((props, ref) => (
    <div
      ref={ref}
      style={{ flexGrow: 1 }}
    >
      <RouterLink {...props} />
    </div>
));   
  

export const CustomerSidebar = ({options, open, toggle}) => {

    const classes = useStyles();

    return (
        <Drawer anchor={'right'} open={open} close={toggle} onOpen={toggle} onClose={toggle}>
            <div className={clsx(classes.list)}
            role="presentation">
                <List>
                    {options.map((option) => (
                    <ListItem button key={option.name}>
                        <Button
                            activeClassName={classes.active}
                            className={classes.button}
                            component={CustomRouterLink}
                            to={option.href}
                        >
                            {option.name}
                        </Button>
                    </ListItem>
                    ))}
                </List>
            </div>
        </Drawer>
    )
}