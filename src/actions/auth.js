import ReactFirebaseInst from '../firebase/firebase';
import Axios from 'axios';
import VARIABLES from '../config';

// login actions
export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

// logout actions
export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

// verification actions
export const VERIFY_REQUEST = "VERIFY_REQUEST";
export const VERIFY_SUCCESS = "VERIFY_SUCCESS";

// Obtain token
export const TOKEN_REQUEST = "TOKEN_REQUEST";
export const TOKEN_SUCCESS = "TOKEN_SUCCESS";
export const TOKEN_FAILED = "TOKEN_FAILED";


// action methods
const requestLogin = () => ({
    type: LOGIN_REQUEST
})

const successLogin = () => ({
    type: LOGIN_SUCCESS,
})

const loginError = () => ({
    type: LOGIN_FAILURE
})



const requestLogout = () => ({
    type: LOGOUT_REQUEST
})

const successLogout = () => ({
    type: LOGOUT_SUCCESS
})

const logoutFailure = () => ({
    type: LOGOUT_FAILURE
})


const verifyRequest = () => ({
    type: VERIFY_REQUEST
})

const verifySuccess = () => ({
    type: VERIFY_SUCCESS
})


const requestToken = () => ({
    type: TOKEN_REQUEST
})

const successToken = token => ({
    type: TOKEN_SUCCESS,
    token
})

const tokenFailure = () => ({
    type: TOKEN_FAILED
})

const userToken = () => (dispatch) => {
    dispatch(requestToken());        
    ReactFirebaseInst.auth().currentUser.getIdToken(true).then((idToken) => {
        dispatch(successToken(idToken))   
    })
    .catch(() => {
        dispatch(tokenFailure())
    })
}


const checkManager = () => (dispatch) => {
    Axios.get(`${VARIABLES.url}/api/checkAdmin`)
    .then(res => {
        // TODO need to continue here 🍕
        console.log(res.data)
    })
    .catch(error => {
        console.log(error)
    })
}


export const loginUser = ({email, password}) => (dispatch) => {
    dispatch(requestLogin());
    ReactFirebaseInst.auth().signInWithEmailAndPassword(email, password)
    .then(user => {
        dispatch(successLogin());
        dispatch(userToken());

        dispatch(checkManager());
    })
    .catch(err => {
        dispatch(loginError())
    })
}


export const logoutUser = (history) => (dispatch) => {
    dispatch(requestLogout())

    ReactFirebaseInst.auth().signOut()
    .then(res => {
        dispatch(successLogout())
        history.push('/login')
    })
    .catch(err => {
        console.log(err)
        dispatch(logoutFailure())
    })
}


export const verifyAuth = () => dispatch => {
    dispatch(verifyRequest()) 
    ReactFirebaseInst.auth().onAuthStateChanged(user => {
        if (user) {
            dispatch(successLogin(user))
        }
        dispatch(verifySuccess())
    })
}

