import axios from 'axios';
import VARIABLES from '../config';

export const CUSTOMER_AUTH_SUCCESS = "CUSTOMER_LOGIN_SUCCESS";
export const CUSTOMER_AUTH_FAILED = "CUSTOMER_LOGIN_FAILED";
export const CUSTOMER_AUTH_REQUEST = "CUSTOMER_LOGIN_REQUEST"

export const customerAuthSuccess = customerId => ({
    type: CUSTOMER_AUTH_SUCCESS,
    customerId
})

export const customerAuthRequest = () => ({
    type: CUSTOMER_AUTH_REQUEST,
})

export const customerAuthFailed = () => ({
    type: CUSTOMER_AUTH_FAILED,
})


export const signupCustomer = (customerObj) => (dispatch) => {

    dispatch(customerAuthRequest())
    // send request to a custmer auth route
    axios.post(VARIABLES.url + '/api/customer', customerObj)
    .then(res => {
        console.log(res)
        dispatch(customerAuthSuccess(res.data.customerId))
    }).catch(() => {
        dispatch(customerAuthFailed())  
    })    
}