import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import {BlindOrDeaf} from './components';
import {InitialForm} from './components';
import { connect } from 'react-redux';
import { signupCustomer } from '../../../actions';
import { useAlertDialog } from '../ProviderCustomAlerts';



const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


const SignupForm = ({openSignUp, handleClose, dispatch}) => {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);

    const [open, setOpen] = React.useState(false);
    const [disabled, setDisabled] = React.useState(true);

    const [customerObj, setCustomerObj] = React.useState(null);

    const alert = useAlertDialog()

    React.useEffect(() => {
      setOpen(openSignUp)
    }, [openSignUp])

    const handleNext = (e) => {
      setActiveStep(activeStep + 1);
      setDisabled(true);  
    };

    const handleBack = () => {
        setActiveStep(activeStep - 1);
    };


    const updateCustomer = (attr) => {
      
      Object.keys(attr).forEach(item  => {
        setCustomerObj({
          ...customerObj,
          [item]: attr[item]
        })
      })
    }

    const steps = ['Blind or Deaf', 'General Informaiton'];

    function getStepContent(step) {
        switch (step) {
            case 0:
              return <BlindOrDeaf disableBtn={setDisabled} updateCustomer={updateCustomer} />;
            case 1:
              return <InitialForm disableBtn={setDisabled} updateCustomer={updateCustomer} />;
            default:
            throw new Error('Unknown step');
        }
    }


    // SERVER INTERACIONS
    const handleSubmit = () => {
      dispatch(signupCustomer(customerObj))
      alert.launchAlert({
        title: "Security Gesture",
        type: "AuthUpload"
      })
      handleClose()
      setActiveStep(0)
    }

  return (
    <React.Fragment>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              Customer Sign Up
            </Typography>
          </Toolbar>
        </AppBar>
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h4" align="center">
            Welcome to Cups
          </Typography>
          <Typography variant="caption" display="block" align="center" gutterBottom>
            Press the escape button to leave the sign up process
          </Typography>
          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant="h5" gutterBottom>
                  Thank you.
                </Typography>
                <Typography variant="subtitle1">
                  You are now a new valued member of CUP cafe. Thank you for joining us
                </Typography>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={handleSubmit}
                    className={classes.button}
                  >
                    Proceed 
                  </Button>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <div className={classes.buttons}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} className={classes.button}>
                      Back
                    </Button>
                  )}
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                    disabled={disabled}
                  >
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </main>
      </Dialog>
    </React.Fragment>
  );
}


const mapStateToProps = state => ({
  customerId: state.customer.customerId
});

export default connect(mapStateToProps)(SignupForm);
