import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Typography, TextField} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}));

const initCustInfo = {
  firstName: '',
  lastName: ''
}

export const InitialForm = ({disableBtn, updateCustomer}) => {
  const classes = useStyles();
  const [customerInfo, setCustomerInfo] = React.useState(initCustInfo)

  const handleChange = (event) => {
    const {name, value} = event.target;
    setCustomerInfo({
      ...customerInfo, 
      [name]: value
    })

    updateCustomer({
      [name]: value
    })
  }

  disableBtn(() => {
    let dsb = true
    Object.values(customerInfo).forEach(value => {
      if (value.length === 0) {
        dsb = true
      } else {
        dsb = false
      }
    })

    return dsb
  });

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        User Information
      </Typography>
      <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                color="secondary"
                id="firstName"
                label="First Name"
                autoFocus
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                color="secondary"
                name="lastName"
                autoComplete="lname"
                onChange={handleChange}
              />
            </Grid>
          </Grid>
        </form>
    </React.Fragment>
  );
}
