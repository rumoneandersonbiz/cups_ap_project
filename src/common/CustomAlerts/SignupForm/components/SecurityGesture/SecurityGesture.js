import React from 'react';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import VARIABLES from '../../../../../config';

import {DropzoneArea} from 'material-ui-dropzone'
import { Grid } from '@material-ui/core';

export const SecurityGesture = ({disableBtn, updateCustomer}) => {

  const saveFile = files => {
    if (files.length > 0) {
      const file = new FormData();
      file.append('samplefile', files[0], files[0].name)
      axios.post(VARIABLES.url+'/api/upload', file, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
        // update to display appropriate message on submit
      }).then(console.log)
      .catch(console.log)
    }
  }

  const handleDelete = () => {
    console.log("image deleted")
  }

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Upload your security Gesture
      </Typography>
      <Grid item xs={12}>
        <DropzoneArea 
          onChange={saveFile}
          onDelete={handleDelete}
          showPreviews={true}
          showPreviewsInDropzone={false}
        />
      </Grid>
      
    </React.Fragment>
  );
}
