import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    disability: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        fontWeight: 'bold'
    },
    optionBtn: {
        margin: theme.spacing(2)
    },
}))


export const BlindOrDeaf = ({disableBtn, updateCustomer}) => {
    const classes = useStyles();

    const [blindOrDeaf, setBlindOrDeaf] = React.useState("");

    const handleChoice = (event) => {
        const disability = (event.target.innerText.toUpperCase() === "YES")? "DEAF": "BLIND"
        setBlindOrDeaf(event.target.innerText)
        updateCustomer({disability})
        disableBtn(false)
    }

    return (
    <React.Fragment>
        <Typography variant="h6" gutterBottom>
            Lets first Identify your disability
        </Typography>
        <Typography className={classes.disability} component="h1" variant="h2" align="center">
            Can you see this?
        </Typography>
        <div align="center">
            <Button variant={(blindOrDeaf.toUpperCase() === 'YES')? 'contained': 'outlined'} className={classes.optionBtn} onClick={handleChoice} color="secondary" size="large" >
                Yes
            </Button>

            <Button variant={(blindOrDeaf.toUpperCase() === 'NO')? 'contained': 'outlined'} className={classes.optionBtn} onClick={handleChoice} color="secondary" size="large" >
                No
            </Button>
        </div>
    </React.Fragment>
  );
}
