import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Axios from 'axios';
import VARIABLES from '../../config';
import { Typography } from '@material-ui/core';

// TODO refactor update manager


const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const UpdateManager = ({toggle}) => {
  const classes = useStyles();

  const [manager, setManager] = React.useState({})
  const [error, setError] = React.useState()

  const update = async () => {
    try {
        const res = await Axios.post(`${VARIABLES.url}/api/manager`, manager)
        setError('')
        toggle()
    } catch (error) {
        setError('An error occured while updating manager please check informaiton inputted')
    }
  }

  const handleChange = (event) => {
    const {name, value} = event.target
    console.log(name, value)
    setManager({
        ...manager,
        [name]: value  
    })
  }

  return (
      <div className={classes.paper}>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                onChange={handleChange}
                id="firstName"
                label="First Name"
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                onChange={handleChange}
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                onChange={handleChange}
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
          </Grid>
          <Button
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={update}
          >
            Update
          </Button>
        </form>
        <Typography variant="caption" display="block" style={{color: 'red'}} gutterBottom>
            {error}
        </Typography>
      </div>
  );
}
