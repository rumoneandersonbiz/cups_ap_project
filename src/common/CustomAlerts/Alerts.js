
import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { UploadDialog } from './UploadDialog';
import { UpdateManager } from './UpdateManager';
import { SignupForm } from './SignupForm';
import { ProviderCustomAlerts } from './ProviderCustomAlerts';
import { IconButton, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import _ from 'lodash/core';



export const AlertDialog = ({ open, toggle, title, body, type }) => {
	const dialogSwitcher = () => {
		switch (type) {
			case "Manager":
				return (
					<DialogContent>
						<UpdateManager toggle={toggle} />
					</DialogContent>
				)
			case "AuthUpload":
				return (
					<DialogContent>
						<UploadDialog toggle={toggle} />
					</DialogContent>
				)
			default:
				return (
					<DialogContent>
						<Typography variant="subtitle1" gutterBottom>
							{body}
						</Typography>
					</DialogContent>
				)

		}
	}

	// TODO fix title

	return (
		<React.Fragment>
			{_.isEqual(type, 'signup') ? (
				<ProviderCustomAlerts>
					<SignupForm openSignUp={open} handleClose={toggle} />
				</ProviderCustomAlerts>
			) : (
					<Dialog
						open={open}
						fullWidth={true}
						maxWidth={'sm'}
						onClose={toggle}
						aria-labelledby="alert-dialog-title"
						aria-describedby="alert-dialog-description"
					>
						<DialogTitle style={{paddingBottom: '0'}} id="alert-dialog-title">
							<IconButton onClick={toggle} aria-label="settings">
								{/* TODO conditional */}
								<CloseIcon />
							</IconButton>
							{title}
						</DialogTitle>
						<DialogContent>
							{dialogSwitcher()}
						</DialogContent>
					</Dialog>
				)}
		</React.Fragment>
	)
}
