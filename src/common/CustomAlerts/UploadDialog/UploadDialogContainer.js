import React, {useState, useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';

import { customerAuthSuccess, customerAuthFailed, customerAuthRequest } from '../../../actions/customer';
import { TooMuchFilesError } from '../../../utils/Errors';
import { POST_IMAGE_CUSTOMER_AUTH } from '../../../constants';
import { ApiCall } from '../../../utils';
import { UploadDialog } from './UploadDialog';



// this component updates a customer's auth image on the server

export const UploadDialogContainer = ({ toggle }) => {

	const [file, setFile] = useState({});
	const [error, setError] = useState('');

	// redux hooks
	const customerId = useSelector(state => state.customer.customerId);
	const dispatch = useDispatch();

	useEffect(() => () => {
		URL.revokeObjectURL(file.preview);
	}, [file]);

	const addImage = (name, file) => {		
		try {
			if (file.length > 1) {
				throw new TooMuchFilesError('Only one image required');
			}

			const firstfile = file[0];
			setError('')
			setFile(Object.assign(firstfile, {preview: URL.createObjectURL(firstfile)}))

		} catch (error) {
			if (_.isEqual(error.name, 'TooMuchFilesError')) {
				setError(error.message)
			} else {
				setError('There was an error with the file uploaded')
			}
		}
	}

	const uploadFile = async () => {
		const Api = new ApiCall(`${POST_IMAGE_CUSTOMER_AUTH}/${customerId}`);
		dispatch(customerAuthRequest);
		const result = await Api.postFile('gesture', file);

		if (_.isObject(result)) {
			dispatch(customerAuthSuccess(result.customerId))
			toggle();
		} else {
			dispatch(customerAuthFailed())
			setError(result)
		}
	}

	// TODO use password along with authentication image
    const UploadDialogProps = {
        dropzoneName: 'gesture',
        fileType: 'image/jpeg, image/png',
        filePreview: file.preview,
        placeholder: 'Click or drag authentication image',
        addFile: addImage,
        save: uploadFile,
		error,
		disable: _.isEmpty(file)
    }

	return (
        <UploadDialog {...UploadDialogProps} />
    );
}
