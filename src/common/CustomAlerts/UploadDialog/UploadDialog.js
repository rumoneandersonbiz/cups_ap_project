import React from 'react';
import { Typography, Button } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { OrderDropzone } from '../../../components';

export const UploadDialog = (props) => (
	<React.Fragment>
		<OrderDropzone {...props} />

		<Typography variant="caption" style={{ color: 'red', margin: '1em' }} display="block" gutterBottom>
			{props.error}
		</Typography>
		<Button
			variant="contained"
			color="secondary"
			size="small"
			style={{ margin: '1em' }}
			startIcon={<SaveIcon />}
			onClick={props.save}
			disabled={props.disable}
		>
			Save
		</Button>
		<Typography variant="caption" gutterBottom>
			When you have selected an image press save to update your account
		</Typography>
	</React.Fragment>
);

