import React from 'react';
import {AlertDialog} from './Alerts';

// initializes the context
const AlertContext = React.createContext(); 

export const useAlertDialog = () => React.useContext(AlertContext)

export const ProviderCustomAlerts = ({children}) => {
    const [alertState, setAlertState] = React.useState(false);
    const [alertData, setAlertData] = React.useState({
        title: '',
        body: '',
        type: 'default',
    })
    const toggle = () => {
        setAlertState(!alertState)
    }

    const launchAlert = (alertInfo, cb) => {
        if (!alertState) {
            setAlertData(alertInfo);
            toggle();
            if(cb && typeof cb === "function") {
                cb();
            }
        }
    }

    const propObj = {
        open: alertState,
        ...alertData,
        toggle,
    }
    
    return (
        <React.Fragment>
            <AlertDialog {...propObj} />
            <AlertContext.Provider value={{launchAlert}}>
                {children}
            </AlertContext.Provider>
        </React.Fragment>
    )
}



