import React from 'react'
import food from './food.png';
import inventory from './inventory.png';
import order from './Order.png';

const Food = () => <img src={food} alt="Food Icon" width={'29px'}/>
const Inventory = () => <img src={inventory} alt="Inventory Icon" width={'29px'} />
const Order = () => <img src={order} alt="Order Icon" width={'29px'} />


export {
    Food,
    Inventory,
    Order
}