import {axiosInstance} from './AiosInstance';
import {ApiCall} from './ApiCall'


export {
    axiosInstance,
    ApiCall,
}