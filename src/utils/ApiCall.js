import _ from 'lodash/core'
import { NoDataReturnedError } from "./Errors";
import { axiosInstance } from './AiosInstance';

const generalError = 'Something went wrong with the application'

export class ApiCall {
    constructor(_url) {
        this.url = _url
    }

    getData = async () => {
        try {
            const {data} = await axiosInstance.get(this.url);
            
            if(_.isEmpty(data)) {
                throw new NoDataReturnedError();
            }

            return data

        } catch (error) {
            if (_.isEqual(error.name, 'NoDataReturnedError')) {
                return error.message
            } else {
                return generalError
            }
        }        
    }


    // file uploads
    // all post requests with files must be uploaded as a FormData() objec and with a heading of content-type: multipart form-data
    // name: name of the file used on the server
    // file: the actual file as an object
    postFile = async (name, file) => {
        try {
            const fileUploadObj = new FormData();
            fileUploadObj.append([name], file)
            
            const {data} = await axiosInstance.post(this.url, fileUploadObj, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            if(_.isEmpty(data)) {
                throw new NoDataReturnedError();
            } 

            return data;
        } catch (error) {
            if (_.isEqual(error.name, 'NoDataReturnedError')) {
                return error.message
            } else {
                return generalError
            }
        }
    }

    // accepts multiple files, convert them to one FormData object and send to the server
    postFiles = async (Arr) => {
        try {
            const fileUploadObj = formatFilesFormData(Arr)
            
            const {data} = await axiosInstance.post(this.url, fileUploadObj, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            if(_.isEmpty(data)) {
                throw new NoDataReturnedError();
            } 

            return data;
        } catch (error) {
            if (_.isEqual(error.name, 'NoDataReturnedError')) {
                return error.message
            } else {
                return generalError
            }
        }
    }
}


const formatFilesFormData = (Arr) => {
    let f = new FormData();
    Arr.forEach(element =>  {
        f.append([element.name], element.file)
    })

    return f;
}