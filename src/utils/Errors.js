// this is a class with custome errors in the application


// Used when request returns empty data structure
export class NoDataReturnedError extends Error {
    constructor() {
        super('No data was returned from API endpoint')
        this.name = 'NoDataReturnedError';
        this.message = 'No data was returned from API endpoint';
    }
}

export class TooMuchFilesError extends Error {
    constructor(message) {
        super(message)
        this.name = 'TooMuchFilesError';
        this.message = message;
    }
}


export class AuthMediaIncorrect extends Error {
    constructor() {
        super('Authentication media submitted was incorrect')
        this.name = 'AuthMediaIncorrect';
        this.message = 'Authentication media submitted was incorrect'
    }
}