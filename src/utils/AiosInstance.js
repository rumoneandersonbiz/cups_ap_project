import axios from "axios";
import VARIABLES from "../config";

export const axiosInstance = axios.create({
    baseURL: VARIABLES.url 
})