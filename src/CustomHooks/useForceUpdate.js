import {useState} from 'react';

export const useForceUpdate = () => {
    const [tickRenderer, setTickRederer] = useState(0);
    return () => setTickRederer(tickRenderer => ++tickRenderer); 
}